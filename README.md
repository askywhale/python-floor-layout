# Python floor layout


A script to find an ideal floor layout for rectangle tiles 

Limits :
- only rectangle or sum-of-rectangle floor
- only all-the-same tiles
- regular tiling or odd/even shift tiling

# How to use
- install Python
- get the script here
- modify it : constants at the begining
- run it : $ python floor_layout.py


[Screenshot](ss.png)

