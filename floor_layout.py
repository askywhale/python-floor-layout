"""
Python floor layout
Copyright (C) 2024, askywhale

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# change those :
# sizes in mm
ground_rects = [
    {"x1":0,"y1":0,"x2":890,"y2":1400},
    {"x1":890,"y1":0,"x2":1880,"y2":1700},
    {"x1":1880,"y1":0,"x2":2500,"y2":1400},
]
tile_size = {"x": 602, "y":401}
seal = 2
shift_odd = {"x": 301, "y": 0} # 0 ... tile_size
search_step = 1 # higher : faster
min_slice = 20 # min slice to be done cutting a tile : if nothing is impossible, set 0
penalty_small_slice = 5 # an impossible cut is 5x worse than a normal cut ; see min_slice

from PIL import Image, ImageDraw, ImageFont

def enclosing_rect(rects):
    res = {"x1":10000,"y1":10000,"x2":-10000,"y2":-10000}
    for rect in rects:
        res["x1"] = min(res["x1"], rect["x1"])
        res["y1"] = min(res["y1"], rect["y1"])
        res["x2"] = max(res["x2"], rect["x2"])
        res["y2"] = max(res["y2"], rect["y2"])
    return res
    
def with_padding(rect, padding):
    return {"x1":rect["x1"]-padding, "y1":rect["y1"]-padding,
        "x2":rect["x2"]+padding,"y2":rect["y2"]+padding}

def intersection(r1, r2):
    res = {"x1":max(r1["x1"], r2["x1"]), "y1":max(r1["y1"], r2["y1"]),
        "x2":min(r1["x2"], r2["x2"]), "y2":min(r1["y2"], r2["y2"]) }
    if (res["x1"] >= res["x2"]) or (res["y1"] >= res["y2"]):
        return None
    else:
        return res

def intersection_rects(rects, target_rect):
    res = []
    for rect in rects:
        this_int = intersection(rect, target_rect)
        if this_int!=None:
            appended = False
            for r in res:
                if (r["x1"]==this_int["x1"]) and (r["x2"]==this_int["x2"]) and (r["y1"]==this_int["y2"]):
                    res.append( {"x1":r["x1"], "y1":this_int["y1"], "x2":r["x2"], "y2":r["y2"]} )
                    res.remove(r)
                    appended = True
                elif (r["x1"]==this_int["x1"]) and (r["x2"]==this_int["x2"]) and (r["y2"]==this_int["y1"]):
                    res.append( {"x1":r["x1"], "y1":r["y1"], "x2":r["x2"], "y2":this_int["y2"]} )
                    res.remove(r)
                    appended = True
                elif (r["y1"]==this_int["y1"]) and (r["y2"]==this_int["y2"]) and (r["x1"]==this_int["x2"]):
                    res.append( {"x1":this_int["x1"], "y1":r["y1"], "x2":r["x2"], "y2":r["y2"]} )
                    res.remove(r)
                    appended = True
                elif (r["y1"]==this_int["y1"]) and (r["y2"]==this_int["y2"]) and (r["x2"]==this_int["x1"]):
                    res.append( {"x1":r["x1"], "y1":r["y1"], "x2":this_int["x2"], "y2":r["y2"]} )
                    res.remove(r)
                    appended = True
            if not appended:
                res.append(this_int)
    return res
    
def contains(rects, target_rect):
    for rect in rects:
        if intersection(rect, target_rect)!=None:
            return True
    return False
    
def area(rect):
    return (rect["x1"]-rect["x2"])*(rect["y1"]-rect["y2"])

def areas(rects):
    res = 0
    for r in rects:
        res += area(r)
    return res


ground = with_padding(enclosing_rect(ground_rects), 1000)
tile_area = tile_size["x"] * tile_size["y"]

min_defects = 10000
best_nb_cuts = None
best_nb_full = None
best_tiles = None
best_offset = None
best_impossible_cuts = None

for offset_y in range(-tile_size["y"]*2+1, 0, search_step):
    for offset_x in range(-tile_size["x"]*2+1, 0, search_step):
        tiles = []
        if shift_odd["y"]==0:
            ny = 0
            for y in range(offset_y, ground["y2"], tile_size["y"]+seal):            
                if ny%2==0:
                    shift_x = 0
                else:
                    shift_x = shift_odd["x"]
                for x in range(offset_x-shift_x, ground["x2"], tile_size["x"]+seal):
                    tile = {"x1":x,"y1":y,"x2":x+tile_size["x"],"y2":y+tile_size["y"]}
                    if contains(ground_rects, tile):
                        tiles.append(tile)
                ny += 1
        else:
            nx = 0
            for x in range(offset_x, ground["x2"], tile_size["x"]+seal):
                if nx%2==0:
                    shift_y = 0
                else:
                    shift_y = shift_odd["y"]
                for y in range(offset_y-shift_y, ground["y2"], tile_size["y"]+seal):            
                    tile = {"x1":x,"y1":y,"x2":x+tile_size["x"],"y2":y+tile_size["y"]}
                    if contains(ground_rects, tile):
                        tiles.append(tile)
                nx += 1
        
        nb_full = 0
        nb_cuts = 0
        nb_impossible_cuts = 0
        n = 1
        for tile in tiles:
            i_rects = intersection_rects(ground_rects, tile)
            if len(i_rects)==1 and area(i_rects[0]) == tile_area:
                nb_full += 1
            else:
                for i_rect in i_rects:
                    if i_rect["x2"]-i_rect["x1"]!=tile_size["x"]:
                        nb_cuts += 1
                    if i_rect["y2"]-i_rect["y1"]!=tile_size["y"]:
                        nb_cuts += 1
                if (i_rect["x2"]-i_rect["x1"]<min_slice or i_rect["y2"]-i_rect["y1"]<min_slice or 
                        (i_rect["x2"]-i_rect["x1"]!=tile_size["x"] and i_rect["x2"]-i_rect["x1"]>tile_size["x"]-min_slice) or (i_rect["y2"]-i_rect["y1"]!=tile_size["y"] and i_rect["y2"]-i_rect["y1"]>tile_size["y"]-min_slice)):
                    nb_impossible_cuts += 1
            n+=1
        if nb_cuts+nb_impossible_cuts*penalty_small_slice < min_defects:
            min_defects = nb_cuts+nb_impossible_cuts*penalty_small_slice
            best_nb_cuts = nb_cuts
            best_nb_full = nb_full
            best_tiles = tiles
            best_offset = {"x": offset_x, "y": offset_y}
            best_impossible_cuts = nb_impossible_cuts

nb_cuts = best_nb_cuts
nb_impossible_cuts = best_impossible_cuts
nb_full = best_nb_full
tiles = best_tiles
offset = best_offset

n = 1
for tile in tiles:
    i_rects = intersection_rects(ground_rects, tile)
    if len(i_rects)==1 and area(i_rects[0]) == tile_area:
        infos = "full"
    else:
        r_sizes = []
        for i_rect in i_rects:
            r_sizes.append( {"x": i_rect["x2"]-i_rect["x1"], "y":i_rect["y2"]-i_rect["y1"]} )
        infos = str(r_sizes)
    print("{0} : {1}".format(n, infos))
    n+=1
print("Full tiles : {0}, cuts : {1}, impossible cuts : {2}".format(nb_full, nb_cuts, nb_impossible_cuts))
print("Areas - ground : {0:.1f}m², tiles : {1:.1f}m², worst extra : {2:d}%".format(
    areas(ground_rects)/1e6, len(tiles)*tile_size["x"]*tile_size["y"]/1e6, 
    int(100*((len(tiles)*tile_size["x"]*tile_size["y"]/areas(ground_rects))-1)) ))

img = Image.new(mode="RGB", size=(ground["x2"]-ground["x1"], ground["y2"]-ground["y1"]))
draw = ImageDraw.Draw(img)
for rect in ground_rects:
    draw.rectangle((rect["x1"]-ground["x1"], rect["y1"]-ground["y1"], 
        rect["x2"]-ground["x1"], rect["y2"]-ground["y1"]), fill=(200,200,200))
n = 1
for tile in tiles:
    draw.rectangle((tile["x1"]-ground["x1"], tile["y1"]-ground["y1"], 
        tile["x2"]-ground["x1"], tile["y2"]-ground["y1"]), width=10, outline=(255,0,255))
    font = ImageFont.load_default(size=tile_size["x"]//5)
    draw.text((tile["x1"]-ground["x1"]+tile_size["x"]//2, tile["y1"]-ground["y1"]+tile_size["y"]//2),
        text=str(n), fill=(255,0,255), font=font)
    n+=1
img.show()